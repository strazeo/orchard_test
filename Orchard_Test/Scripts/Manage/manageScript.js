﻿$(function () {

    var root = 'https://jsonplaceholder.typicode.com';

    $.ajax({
        url: root + '/posts?userId=' + userId,
        method: 'GET'
    }).then(function (data) {
        var html = "<ul>";
        $.each(data.slice(0, 5), function (index, item) {
            html += "<li>";
            html += "<h2>" + item.title + "</h2>";
            html += "<p>" + item.body + "</p>";
            html += "</li>";
        });
        html += "</ul>";
        $("#userPosts").html(html);
    });
});

function addPost() {
    var form = $('#__AjaxAntiForgeryForm');
    var token = $('input[name="__RequestVerificationToken"]', form).val();
    $.ajax({
        url: postUrl,
        method: 'POST',
        dataType: 'json',
        data: {
            __RequestVerificationToken: token, 
            userId: userId, title: "The Title!", body: "Amazing Body"
        }
    }).then(function (data) {
        $('#postResult').html(data);
    })
}