﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Orchard_Test.Startup))]
namespace Orchard_Test
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
