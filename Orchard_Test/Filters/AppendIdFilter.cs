﻿using System.Web.Mvc;

namespace Orchard_Test.Filters
{
    public class AppendIdFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.ActionParameters.Keys.Contains("id") && filterContext.ActionParameters["id"] == null)
            {
                filterContext.ActionParameters["id"] = "1";
            }
            base.OnActionExecuting(filterContext);
        }
    }
}