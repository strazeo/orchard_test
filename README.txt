I solved most of the AJAX calls using simple JQuery and didn't add any JS framework for binding.
I didn't implement the "Load More" functionality / Caching due to time constraints.
Obviously having a database will make persisting objects easy.

I kept most of the default MVC template code intact, so might be a bit of a hassle to find the parts I changed.
Let me know if that's the case and I'll remove the unused code.